﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ReplaceObjMenu 
{
    [MenuItem("Matthijs/Tools/Replacer")]
    public static void ReplaceObj()
    {
        ReplaceObjEditor.LaunchEditor();
    }
}
