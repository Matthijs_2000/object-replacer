﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ReplaceObjEditor : EditorWindow
{
    int selectionCount = 0;
    GameObject wantedObject;

    public static void LaunchEditor()
    {
        var editorWindow = GetWindow<ReplaceObjEditor>("Replace Objects");
        editorWindow.Show();
    }

    private void OnGUI()
    {
        //How much GameObj are selected
        GetSelection();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Selection Count:" + selectionCount.ToString(), EditorStyles.boldLabel);
        EditorGUILayout.Space();

        wantedObject = (GameObject)EditorGUILayout.ObjectField("Replace Object: ", wantedObject, typeof(GameObject), true);
        if(GUILayout.Button("Replace Selected Objects", GUILayout.ExpandWidth(true), GUILayout.Height(40)))
        {
            ReplaceSelectedObjects();
        }

        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();

        Repaint();
    }

    void GetSelection()
    {
        selectionCount = 0;
        selectionCount = Selection.gameObjects.Length;
    }

    void ReplaceSelectedObjects()
    {
        if(selectionCount == 0)
        {
            CustomDialog("At leat one object needs to be selected!");
            return;
        }

        if(!wantedObject)
        {
            CustomDialog("The replace object id empty, please assign something!");
            return;
        }

        //Replace Objects
        GameObject[] selectedObjects = Selection.gameObjects;
        for(int i = 0; i < selectedObjects.Length; i++)
        {
            Transform selectTransform = selectedObjects[i].transform;
            GameObject newObject = Instantiate(wantedObject, selectTransform.position, selectTransform.rotation);
            newObject.transform.localScale = selectTransform.localScale;

            DestroyImmediate(selectedObjects[i]);
        }
    }

    void CustomDialog(string aMessage)
    {
        EditorUtility.DisplayDialog("Replace Objects Warning", aMessage, "OK");
    }
}
